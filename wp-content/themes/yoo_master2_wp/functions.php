<?php
/**
* @package   Master
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// check compatibility
if (version_compare(PHP_VERSION, '5.3', '>=')) {

    // bootstrap warp
    require(__DIR__.'/warp.php');
}

function delete_between_strings($string, $start, $end = null ) {
    $start_position = strpos( $string, $start );
    if( is_null( $end ) ) {
        $deleted_text = substr( $string, $start_position );
    } else {
        $end_position = strpos( $string, $end );
        if( $start_position === false || $end_position === false ) return $string;
        $deleted_text = substr( $string, $start_position, ( $end_position + strlen( $end ) ) - $start_position );	
    }
    return str_replace( $deleted_text, '', $string );
}

function insert_before_article_end($content, $newContent) {
    $results = explode('</article>', $content);
    return $results[0].'<div class="youtube-video">'.$newContent.'</div></article>';
}

function create_stylist_profile($post, $index) { 
    $direction = $index % 2 ? 'left' : 'right';
    
    $bio = '<div class="stylist-bio-'.$direction.'" style=""><h2 class="purple-text">'.get_post_meta($post->ID, 'stylist-'.$index.'-name', true).'</h2>'
            .get_post_meta($post->ID, 'stylist-'.$index.'-bio', true).'</div>';
    $image = '<div class="stylist-image"><img src="'.get_post_meta($post->ID, 'stylist-'.$index.'-image', true).'" /></div>';
    $row = $index % 2 ? $bio.$image : $image.$bio;
    return '<div class="uk-clearkfix stylist-container">'.$row.'</div>';
}