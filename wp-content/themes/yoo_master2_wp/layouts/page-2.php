<?php
/**
 * @package   Master
 * @author    YOOtheme http://www.yootheme.com
 * @copyright Copyright (C) YOOtheme GmbH
 * @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
 */
// get theme configuration
include($this['path']->path('layouts:theme.config.php'));
require_once 'kint-master/Kint.class.php';

?>
<!DOCTYPE HTML>
<html lang="<?php echo $this['config']->get('language'); ?>" dir="<?php echo $this['config']->get('direction'); ?>"  data-config='<?php echo $this['config']->get('body_config', '{}'); ?>'>

    <head>
        <?php echo $this['template']->render('head'); ?>
    </head>
<?php global $post; ?>
    <body class="<?php echo $this['config']->get('body_classes'); ?>">

        <div class="uk-container uk-container-center">

            <?php if ($this['widgets']->count('toolbar-l + toolbar-r')) : ?>
                <div class="tm-toolbar uk-clearfix uk-hidden-small">

                    <?php if ($this['widgets']->count('toolbar-l')) : ?>
                        <div class="uk-float-left"><?php echo $this['widgets']->render('toolbar-l'); ?></div>
                    <?php endif; ?>

                    <?php if ($this['widgets']->count('toolbar-r')) : ?>
                        <div class="uk-float-right"><?php echo $this['widgets']->render('toolbar-r'); ?></div>
                    <?php endif; ?>

                </div>
            <?php endif; ?>  

            <?php if ($this['widgets']->count('logo + headerbar')) : ?>
                <div class="tm-headerbar uk-clearfix uk-hidden-small">

                    <?php if ($this['widgets']->count('logo')) : ?>
                        <a class="tm-logo" href="<?php echo $this['config']->get('site_url'); ?>"><?php echo $this['widgets']->render('logo'); ?></a>
                    <?php endif; ?>

                    <?php echo $this['widgets']->render('headerbar'); ?>

                </div>
            <?php endif; ?>

            <?php if ($this['widgets']->count('menu + search')) : ?>
                <nav class="tm-navbar uk-navbar">

                    <?php if ($this['widgets']->count('menu')) : ?>
                        <?php echo $this['widgets']->render('menu'); ?>
                    <?php endif; ?>

                    <?php if ($this['widgets']->count('offcanvas')) : ?>
                        <a href="#offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>
                    <?php endif; ?>

                    <?php if ($this['widgets']->count('search')) : ?>
                        <div class="uk-navbar-flip">
                            <div class="uk-navbar-content uk-hidden-small">
                                <?php echo $this['widgets']->render('search'); ?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($this['widgets']->count('logo-small')) : ?>
                        <div class="uk-navbar-content uk-navbar-center uk-visible-small">
                            <a class="tm-logo-small" href="<?php echo $this['config']->get('site_url'); ?>">
                                <?php echo $this['widgets']->render('logo-small'); ?>
                            </a>
                        </div>
                    <?php endif; ?>

                </nav>
            <?php endif; ?>


            <?php if ($this['widgets']->count('top-a')) : ?>
                <section class="<?php echo $grid_classes['top-a']; echo $display_classes['top-a']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
                    <?php echo $this['widgets']->render('top-a', array('layout' => $this['config']->get('grid.top-a.layout'))); ?>
                </section>
                <div class="bottom-purple-out">
                    <h1>feel more beautiful than ever</h1>
                </div>
            <?php endif; ?>

            <?php if ($this['widgets']->count('top-b')) : ?>
                <section class="mobile-fix <?php echo $grid_classes['top-b']; echo $display_classes['top-b']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
                    <div class="uk-width-1-2 salon-image-1">

                    </div>
                    <div class="uk-width-1-2 contact">
                        <h1>roots salon <span><img src="http://roots.nickdeckerdevs.com/wp-content/uploads/2015/06/smallrootslogo.png" /></span> cape coral</h1>
                        <div class="padding">
                            <?php echo $this['widgets']->render('top-b', array('layout' => $this['config']->get('grid.top-b.layout'))); ?>  
                        </div>

                        <div class="uk-grid split">
                            <div class="uk-width-1-2">
                                <?php 
                                if(get_post_custom_values('address')) { 
                                    $address = get_post_meta($post->ID, 'address', true);
                                    echo '<h3>address</h3>';
                                    echo $address;
                                }
                                ?>
                            </div>
                            <div class="uk-width-1-2">
                                <?php 
                                if(get_post_custom_values('hours')) { 
                                    $hours = get_post_meta($post->ID, 'hours', true);
                                    echo '<h3>hours</h3>';
                                    echo $hours;
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </section>
                <?php 
                if(get_post_custom_values('teaser-1-title')) { 
                    $title = get_post_meta($post->ID, 'teaser-1-title', true);
                    $teaser_text = get_post_meta($post->ID, 'teaser-1-text', true);
                    $teaser_link = get_post_meta($post->ID, 'teaser-1-link', true);
                    $teaser_background = get_post_meta($post->ID, 'teaser-1-background', true);
                    ?>
                    <style>.large-product-cta-teaser { background: url(<?php echo $teaser_background; ?>); }</style>
                    <?php
                }
                ?>
                <?php 
                if(get_post_custom_values('teaser-1-title')) { 
                    ?>
                    <section class="large-product-cta-teaser">
                        <div class="overlay">
                            <h2><?php echo $title; ?></h2>
                            <a href="<?php echo $teaser_link; ?>" class="ghost-cta"><?php echo $teaser_text; ?></a>
                        </div>
                    </section>
                <?php 
                } ?>
            
            <?php endif; ?>

                <?php if ($this['widgets']->count('main-top + main-bottom + sidebar-a + sidebar-b') || $this['config']->get('system_output', true)) : ?>
                <div class="mobile-fix tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin >

                        <?php if ($this['widgets']->count('main-top + main-bottom') || $this['config']->get('system_output', true)) : ?>
                        <div class="<?php echo $columns['main']['class'] ?>">

                            <?php if ($this['widgets']->count('main-top')) : ?>
                                <section class="<?php echo $grid_classes['main-top']; echo $display_classes['main-top']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('main-top', array('layout' => $this['config']->get('grid.main-top.layout'))); ?></section>
                            <?php endif; ?>

                            <?php if ($this['config']->get('system_output', true)) : ?>
                                <main class="tm-content">

                                <?php if ($this['widgets']->count('breadcrumbs')) : ?>
                                    <?php echo $this['widgets']->render('breadcrumbs'); ?>
                                <?php endif; ?>

                                <?php echo $this['template']->render('content'); ?>

                                </main>
                            <?php endif; ?>

                            <?php if ($this['widgets']->count('main-bottom')) : ?>
                                <section class="<?php echo $grid_classes['main-bottom']; echo $display_classes['main-bottom']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('main-bottom', array('layout' => $this['config']->get('grid.main-bottom.layout'))); ?></section>
                        <?php endif; ?>

                        </div>
                    <?php endif; ?>

                    <?php foreach ($columns as $name => &$column) : ?>
                        <?php if ($name != 'main' && $this['widgets']->count($name)) : ?>
                            <aside class="<?php echo $column['class'] ?>"><?php echo $this['widgets']->render($name) ?></aside>
                        <?php endif ?>
                    <?php endforeach ?>

                </div>
                <?php endif; ?>

                <?php if ($this['widgets']->count('bottom-a')) : ?>
                    <section class="<?php echo $grid_classes['bottom-a']; echo $display_classes['bottom-a']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('bottom-a', array('layout' => $this['config']->get('grid.bottom-a.layout'))); ?></section>
                <?php endif; ?>

                <?php if ($this['widgets']->count('bottom-b')) : ?>
                    <section class="<?php echo $grid_classes['bottom-b']; echo $display_classes['bottom-b']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('bottom-b', array('layout' => $this['config']->get('grid.bottom-b.layout'))); ?></section>
                <?php endif; ?>

                <?php if ($this['widgets']->count('footer + debug') || $this['config']->get('warp_branding', true) || $this['config']->get('totop_scroller', true)) : ?>
                    <footer class="tm-footer">

                        <?php if ($this['config']->get('totop_scroller', true)) : ?>
                            <a class="tm-totop-scroller" data-uk-smooth-scroll href="#"></a>
                        <?php endif; ?>

                    <?php echo $this['widgets']->render('footer'); echo $this['widgets']->render('debug'); ?>

                    </footer>
                <?php endif; ?>

        </div>

        <?php echo $this->render('footer'); ?>

        <?php if ($this['widgets']->count('offcanvas')) : ?>
            <div id="offcanvas" class="uk-offcanvas">
                <div class="uk-offcanvas-bar"><?php echo $this['widgets']->render('offcanvas'); ?></div>
            </div>
        <?php endif; ?>
        
<script>
    (function($) {
    var extra = 0;
    checkMobile();
    adjustPurpleBox();

    $('.widget_nav_menu a').each(function() {
        switch ($(this).text()) {
            case 'fb':
                console.log('fb');
                $(this).html('<img src="http://roots.nickdeckerdevs.com/wp-content/uploads/2015/06/facebook.png" />');
                break;
            case 'googleplus':
                $(this).html('<img src="http://roots.nickdeckerdevs.com/wp-content/uploads/2015/06/googleplus.png" />');
                break;
            case 'twitter':
                $(this).html('<img src="http://roots.nickdeckerdevs.com/wp-content/uploads/2015/06/twitter.png" />');
                break;
            case '':
                console.log('no text');
                $(this).remove();
                break;
            default:

        }


    });


    $('#back-to-top').on('click', function() {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    });

    adjustShadowBox($('body.page-id-2 .tm-top-a').outerHeight());
    $(window).on('resize', function() {
        checkMobile();
        backgroundHeight = $('body.page-id-2 .tm-top-a').outerHeight();
        console.log('background height: ' + backgroundHeight);
        adjustShadowBox(backgroundHeight);
        adjustPurpleBox();
        fixLogo();
    });

    function checkMobile() {
        if ($('nav.tm-navbar:visible')) {
            extra = $('nav.tm-navbar:visible').outerHeight();
            console.log(extra);
        }
    }
    function adjustShadowBox(height) {
        console.log(height / 2)
        blackoutHeight = $('.black-out').outerHeight();
        console.log('blackout height:' + blackoutHeight);
        $('.black-out').css('margin-top', (height / 2) - blackoutHeight - extra);
    }

    function adjustPurpleBox() {
        purpleHeight = $('.bottom-purple-out').outerHeight();
        console.log(purpleHeight);
        $('.bottom-purple-out').css('margin-top', '-' + (purpleHeight + extra) + 'px');

    }

    function fixLogo() {
        /* Removed because the height was just tooo big */
//                var newLogoHeight = $('.uk-panel.widget_nav_menu').outerHeight();
//                $('.tm-toolbar .uk-panel.widget_text img').height(newLogoHeight);

    }

})(jQuery);

</script>
</body>
</html>