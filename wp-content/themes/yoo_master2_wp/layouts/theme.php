<?php
/**
* @package   Master
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// get theme configuration
include($this['path']->path('layouts:theme.config.php'));

?>
<!DOCTYPE HTML>
<html lang="<?php echo $this['config']->get('language'); ?>" dir="<?php echo $this['config']->get('direction'); ?>"  data-config='<?php echo $this['config']->get('body_config','{}'); ?>'>

<head>
<?php echo $this['template']->render('head'); ?>
</head>
<?php 
global $post;
$video = [];
if(get_post_custom_values('youtube-1')) { 
    $video[0] = get_post_meta($post->ID, 'youtube-1', true);
    if(get_post_custom_values('youtube-2')) { 
        $video[1] = get_post_meta($post->ID, 'youtube-2', true);
    }
}
?>

<body class="<?php echo $this['config']->get('body_classes'); ?>">

	<div class="uk-container uk-container-center">

		<?php if ($this['widgets']->count('toolbar-l + toolbar-r')) : ?>
		<div class="tm-toolbar uk-clearfix uk-hidden-small">

			<?php if ($this['widgets']->count('toolbar-l')) : ?>
			<div class="uk-float-left"><?php echo $this['widgets']->render('toolbar-l'); ?></div>
			<?php endif; ?>

			<?php if ($this['widgets']->count('toolbar-r')) : ?>
			<div class="uk-float-right"><?php echo $this['widgets']->render('toolbar-r'); ?></div>
			<?php endif; ?>

		</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('logo + headerbar')) : ?>
		<div class="tm-headerbar uk-clearfix uk-hidden-small">

			<?php if ($this['widgets']->count('logo')) : ?>
			<a class="tm-logo" href="<?php echo $this['config']->get('site_url'); ?>"><?php echo $this['widgets']->render('logo'); ?></a>
			<?php endif; ?>

			<?php echo $this['widgets']->render('headerbar'); ?>

		</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('menu + search')) : ?>
		<nav class="tm-navbar uk-navbar">

			<?php if ($this['widgets']->count('menu')) : ?>
			<?php echo $this['widgets']->render('menu'); ?>
			<?php endif; ?>

			<?php if ($this['widgets']->count('offcanvas')) : ?>
			<a href="#offcanvas" class="uk-navbar-toggle uk-visible-small" data-uk-offcanvas></a>
			<?php endif; ?>

			<?php if ($this['widgets']->count('search')) : ?>
			<div class="uk-navbar-flip">
				<div class="uk-navbar-content uk-hidden-small"><?php echo $this['widgets']->render('search'); ?></div>
			</div>
			<?php endif; ?>

			<?php if ($this['widgets']->count('logo-small')) : ?>
			<div class="uk-navbar-content uk-navbar-center uk-visible-small"><a class="tm-logo-small" href="<?php echo $this['config']->get('site_url'); ?>"><?php echo $this['widgets']->render('logo-small'); ?></a></div>
			<?php endif; ?>

		</nav>
		<?php endif; ?>

		<?php if ($this['widgets']->count('main-top + main-bottom + sidebar-a + sidebar-b') || $this['config']->get('system_output', true)) : ?>
		<?php if(get_post_custom_values('page-background')) { ?>
                    <style> 
                        .article-image { 
                            background: 
                            url(<?php echo get_post_meta($post->ID, 'page-background', true); ?>) center center no-repeat;
                            background-size: cover; 
                        } </style>
                <?php } ?>
                <div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>

			<?php if ($this['widgets']->count('main-top + main-bottom') || $this['config']->get('system_output', true)) : ?>
			<div class="<?php echo $columns['main']['class'] ?>">

				<?php if ($this['widgets']->count('main-top')) : ?>
				<section class="<?php echo $grid_classes['main-top']; echo $display_classes['main-top']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('main-top', array('layout'=>$this['config']->get('grid.main-top.layout'))); ?></section>
				<?php endif; ?>

				<?php if ($this['config']->get('system_output', true)) : ?>
				<main class="tm-content">

					<?php if ($this['widgets']->count('breadcrumbs')) : ?>
					<?php echo $this['widgets']->render('breadcrumbs'); ?>
					<?php endif; ?>
                                        <?php 
                                            echo delete_between_strings( $this['template']->render('content'), '<h1 class="uk-article-title">', '</h1>' );
                                        ?>
					<div class="article-image"></div>
                                        
				</main>
                                <?php
                                if($video[0]) {
                                    echo '<div class="uk-clearfix"></div>';
                                    if($video[1]) {
                                        echo '<div class="youtube-video-half">'.$video[0].'</div>';
                                        echo '<div class="youtube-video-half">'.$video[1].'</div>';
                                    }
                                    else {
                                        echo '<div class="youtube-video-full">'.$video[0].'</div>';
                                    }
                                    
                                } 
                                $stylists = [];
                                for($i = 1; $i <= 7; $i++) {
                                    if(get_post_custom_values('stylist-'.$i.'-image')) { 
                                        $stylists[] = create_stylist_profile($post, $i);
                                    }
                                }
                                if($stylists[0]) {
                                    echo '<div class="uk-clearfix"></div>';
                                    echo '<div class="stylists">';
                                    echo '<h1><a name="stylists"></a>Meet our Awesome Stylists</h1>';
                                    foreach($stylists as $stylist) {
                                        echo $stylist;
                                    }
                                    echo '</div>';
                                }
                                
                                ?>
				<?php endif; ?>

				<?php if ($this['widgets']->count('main-bottom')) : ?>
				<section class="<?php echo $grid_classes['main-bottom']; echo $display_classes['main-bottom']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('main-bottom', array('layout'=>$this['config']->get('grid.main-bottom.layout'))); ?></section>
				<?php endif; ?>

			</div>
			<?php endif; ?>
            
            <?php foreach($columns as $name => &$column) : ?>
            <?php if ($name != 'main' && $this['widgets']->count($name)) : ?>
            <aside class="<?php echo $column['class'] ?>"><?php echo $this['widgets']->render($name) ?></aside>
            <?php endif ?>
            <?php endforeach ?>

		</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('bottom-a')) : ?>
		<section class="<?php echo $grid_classes['bottom-a']; echo $display_classes['bottom-a']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('bottom-a', array('layout'=>$this['config']->get('grid.bottom-a.layout'))); ?></section>
		<?php endif; ?>

		<?php if ($this['widgets']->count('bottom-b')) : ?>
		<section class="<?php echo $grid_classes['bottom-b']; echo $display_classes['bottom-b']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('bottom-b', array('layout'=>$this['config']->get('grid.bottom-b.layout'))); ?></section>
		<?php endif; ?>
                
		<?php if ($this['widgets']->count('footer + debug') || $this['config']->get('warp_branding', true) || $this['config']->get('totop_scroller', true)) : ?>
		<footer class="tm-footer">
                        <div class="instagram-container"></div>
			<?php if ($this['config']->get('totop_scroller', true)) : ?>
			<a class="tm-totop-scroller" data-uk-smooth-scroll href="#"></a>
			<?php endif; ?>

			<?php
				echo $this['widgets']->render('footer');
				$this->output('warp_branding');
				echo $this['widgets']->render('debug');
			?>

		</footer>
		<?php endif; ?>

	</div>
        
	<?php echo $this->render('footer'); ?>

	<?php if ($this['widgets']->count('offcanvas')) : ?>
	<div id="offcanvas" class="uk-offcanvas">
		<div class="uk-offcanvas-bar"><?php echo $this['widgets']->render('offcanvas'); ?></div>
	</div>
	<?php endif; ?>
<script>
(function($) {
    var extra = 0;
    checkMobile();
    adjustPurpleBox();
//            fixLogo();
    $('.widget_nav_menu a').each(function() {
        switch ($(this).text()) {
            case 'fb':
                console.log('fb');
                $(this).html('<img src="http://roots.nickdeckerdevs.com/wp-content/uploads/2015/06/facebook.png" />');
//                                .parent().css('padding', '3em .2em').css('margin-left', '3px');
                break;
            case 'googleplus':
                $(this).html('<img src="http://roots.nickdeckerdevs.com/wp-content/uploads/2015/06/googleplus.png" />');
//                                .parent().css('padding', '3em .2em').css('margin-left', '3px');
                break;
            case 'twitter':
                $(this).html('<img src="http://roots.nickdeckerdevs.com/wp-content/uploads/2015/06/twitter.png" />');
//                                .parent().css('padding', '3em .2em').css('margin-left', '3px');
                break;
            case '':
                console.log('no text');
                $(this).remove();
                break;
            default:

        }


    });


    $('#back-to-top').on('click', function() {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    });
    fixArticleBoxes();
    adjustShadowBox($('body.page-id-2 .tm-top-a').outerHeight());
    $('.youtube-video iframe').each(function() {
        resizeVideo($(this));
    });
    getInstagram();
    $(window).on('resize', function() {
        checkMobile();
        backgroundHeight = $('body.page-id-2 .tm-top-a').outerHeight();
        console.log('background height: ' + backgroundHeight);
        adjustShadowBox(backgroundHeight);
        adjustPurpleBox();
        fixLogo();
        fixArticleBoxes();
        $('.youtube-video iframe').each(function() {
            resizeVideo($(this));
        });
    });

    function checkMobile() {
        if ($('nav.tm-navbar:visible')) {
            extra = $('nav.tm-navbar:visible').outerHeight();
            console.log(extra);
        }
    }
    function adjustShadowBox(height) {
        console.log(height / 2)
        blackoutHeight = $('.black-out').outerHeight();
        console.log('blackout height:' + blackoutHeight);
        $('.black-out').css('margin-top', (height / 2) - blackoutHeight - extra);
    }

    function adjustPurpleBox() {
        purpleHeight = $('.bottom-purple-out').outerHeight();
        console.log(purpleHeight);
        $('.bottom-purple-out').css('margin-top', '-' + (purpleHeight + extra) + 'px');

    }
    
    function fixArticleBoxes() {
        articleHeight = $('.uk-article').outerHeight();
        $('.article-image').css('height', articleHeight+'px');
    }

    function fixLogo() {
        /* Removed because the height was just tooo big */
//                var newLogoHeight = $('.uk-panel.widget_nav_menu').outerHeight();
//                $('.tm-toolbar .uk-panel.widget_text img').height(newLogoHeight);

    }
    
    function resizeVideo(iframe) {
        var aspect = iframe.height() / iframe.width();
        var containerWidth = iframe.parent().width();
        var newWidth = containerWidth / 2;
        iframe.width(newWidth - 5).height(newWidth * aspect);
    }
    
    function getRandoms(total) {
        randoms = [];
        for(var i = 0; i <= total; i++) {
            randoms.push(Math.floor(Math.random() * 19) + 1);
        }
        return randoms;
    }
        
    
    function getInstagram() {
        var accessToken = '195985139.a8c4b1b.d677490d61e140b1873cf81ca8e0f0b3';
        var clientId = 'a8c4b1bebcf54bec8b12b7459836b186';
        var userId = '195985139';
        var images = 5;
        var randomImages = getRandoms(images);
        console.log(randomImages);
//        console.log(randomImages[0])
//        console.log(randomImages[1])
//        console.log(randomImages[2])
        $.ajax({
            type: "GET",
            dataType: "jsonp",
            cache: false,
            url: "https://api.instagram.com/v1/users/"+userId+"/media/recent/?access_token="+accessToken,
            success: function(response) {
                console.log(response);
                for(var j=0; j < randomImages.length; j++) {
                    var imageIndex = randomImages[j];
                    console.log(imageIndex);
                    console.log(response.data[imageIndex].images.standard_resolution.url);
                    $('.instagram-container').append('<img src="'+response.data[imageIndex].images.standard_resolution.url+'" />');
                }
                var imageCount = $('.instagram-container').length;
                console.log('aaacount'+imageCount)
                var imageWidth = 100 / (images + 1);
                console.log(imageWidth);
                $('.instagram-container img').each(function() {
                    $(this).css('width', imageWidth+'%');
                    
                });

            }
        });
    }
})(jQuery);
</script>
</body>
</html>